(function ($) {

	/*==========FOR SVG ==============*/
	svg4everybody();

	/*FOR MENU*/
$('.burger').click(function(){
	$(this)
	.find('.icon')
	.toggleClass('menu')
	.toggleClass('close');

	$('#menu').toggleClass('open');
	$('body').toggleClass('menu_open');
	$('.header-back').toggleClass('open');
});


$('.header-back').click(function(){
	$(this).removeClass('open');
	$('.burger')
	.find('.icon')
	.toggleClass('menu')
	.toggleClass('close');

	$('#menu').removeClass('open');
	$('body').removeClass('menu_open');
});

	/*----------------------------------------
	 SLICK CAROUSELS
	 ----------------------------------------*/
	$('.slider').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2
			}
		},
		{
			breakpoint: 481,
			settings: {
				slidesToShow: 1
			}
		},
		{
			breakpoint: 980,
			settings: {
				slidesToShow: 3
			}
		},
		{
			breakpoint: 1050,
			settings: {
				slidesToShow: 4
			}
		},
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 5
			}
		},
	]
	});


})(jQuery);
